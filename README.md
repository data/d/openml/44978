# OpenML dataset: cpu_activity

https://www.openml.org/d/44978

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**


The Computer Activity databases are a collection of computer systems activity measures. The data was collected from a Sun Sparcstation 20/712 with 128 Mbytes of memory running in a multi-user university department.
Users would typically be doing a large variety of tasks ranging from accessing the internet, editing files or running very cpu-bound programs. The data was collected continuously on two separate occasions. On both occassions, system activity was gathered every 5 seconds. The final dataset is taken from both occasions with equal numbers of observations coming from each collection epoch.

Each instance represents one observed system performance. The goal is to predict portion of time that cpus run in user mode.

**Attribute Description**

1. *lread* - Reads (transfers per second ) between system memory and user memory.
2. *lwrite* - writes (transfers per second) between system memory and user memory.
3. *scall* - Number of system calls of all types per second.
4. *sread* - Number of system read calls per second.
5. *swrite* - Number of system write calls per second .
6. *fork* - Number of system fork calls per second.
7. *exec* - Number of system exec calls per second.
8. *rchar* - Number of characters transferred per second by system read calls.
9. *wchar* - Number of characters transfreed per second by system write calls.
10. *pgout* - Number of page out requests per second.
11. *ppgout* - Number of pages, paged out per second.
12. *pgfree* - Number of pages per second placed on the free list.
13. *pgscan* - Number of pages checked if they can be freed per second.
14. *atch* - Number of page attaches (satisfying a page fault by reclaiming a page in memory) per second.
15. *pgin* - Number of page-in requests per second.
16. *ppgin* - Number of pages paged in per second.
17. *pflt* - Number of page faults caused by protection errors (copy-on-writes).
18. *vflt* - Number of page faults caused by address translation.
19. *runqsz* - Process run queue size.
20. *freemem* - Number of memory pages available to user processes.
21. *freeswap* - Number of disk blocks available for page swapping.
22. *usr* - Portion of time (%) that cpus run in user mode.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44978) of an [OpenML dataset](https://www.openml.org/d/44978). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44978/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44978/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44978/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

